<?php

namespace App\Http\Controllers;

use App\Compliant;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
class ComplaintController extends Controller
{
    public function index(Request $request){
        if($request->isJson()){
            $compliants = Compliant::all();
            return response() -> json($compliants,200);
        }
        return response() -> json(['error' => 'Unauthorized'],401);
    }

    public function createCompliant(Request $request){
        if($request-> isJson()){
            $data = $request->json()->all();
            $compliant = Compliant::create([
                'title' => $data['title'],
                'description' => $data['description'],
                'like' => 0,
                'unlike' => 0,
                'score' => 0
            ]);
            return response() -> json($compliant,201);
        }
        return response() -> json(['error' => 'Unauthorized'],401);
    }

    public function setLike(Request $request){
        if($request -> isJson()){
            $data = $request->json()->all();
            $compliant = Compliant::find($data['id']);
            $compliant->like = $compliant->like + 1;
            $compliant->score = $compliant->score + 1;
            $compliant->save();
            return response() -> json($compliant,202);
        }
        return response() -> json(['error' => 'Unauthorized'],401);
    }


    public function setUnlike(Request $request){
        if($request -> isJson()){
            $data = $request->json()->all();
            $compliant = Compliant::find($data['id']);
            $compliant->unlike = $compliant->unlike + 1;
            $compliant->score = $compliant->score - 1;
            $compliant->save();
            return response() -> json($compliant,202);
        }
        return response() -> json(['error' => 'Unauthorized'],401);
    }
}
